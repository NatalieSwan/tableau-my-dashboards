# Dashboards in Tableau
Здесь я собрала ссылки на мои работы, которые можно увидеть на [Tableau public](https://public.tableau.com/app/profile/natalie.swan3088/vizzes).

1. [Анализ удовлетворённости сотрудников](#1-анализ-удовлетворённости-сотрудников)
2. [Когортный анализ](#2-когортный-анализ)
3. [Обзор зарплат и требуемых навыков для аналитиков](#3-обзор-зарплат-и-требуемых-навыков-для-аналитиков)
4. [Profits overview](#4-profits-overview)

Далее подробнее о каждом дашборде

Если у вас не подгружаются скрины, их можно просмотреть в папке [Screenshots](https://gitlab.com/data-visualization8525117/dashboards-in-tableau/-/tree/main/screenshots?ref_type=heads) в этом репозитории.

## 1. Анализ удовлетворённости сотрудников
Дашборд [Employee Satisfaction](https://goo.su/yrL7f) разработан по заданию отдела HR аналитики.

**Цель** - понять косвенно, через уровень удовлетворенности работников, насколько они близки к выгоранию, чтобы при необхоимости предотвратить его.


**Dataset**

Сотрудников попросили оценить удовлетворенность работой, обстановкой на работе, отношениями с коллегами, баланс работы и личной жизни. 

Результаты опроса собрали в Excel [Hr data_satisfaction](https://gitlab.com/data-visualization8525117/dashboards-in-tableau/-/blob/main/Hr%20data_satisfaction.xlsx?ref_type=heads).
В датасете нас интересовали следующие колонки:
* Department - отдел, в котором числится работник
* JobRole - должность
* EnvironmentSatisfaction - уровень удовлетворенности рабочей средой (1-4, где 4 - очень удовлетворен)
* JobSatisfaction - Уровень удовлетворенности работой (1-4, где 4 - очень удовлетворен)
* RelationshipSatisfaction - Уровень удовлетворенности отношениями внутри компании (1-4, где 4 - очень удовлетворен)
* WorkLifeBalance - Уровень удовлетноренности соотношением работы и личной жизни (1-4, где 4 - отличный баланс)
* Факторы для изучения корреляции:
  * MonthlyIncome - Месячный доход
  * TotalWorkingYears - Общий трудовой стаж (в годах)
  * TrainingTimesLastYear - Количество часов повышения квалификации в прошлом году
  * YearsAtCompany - Количество лет в компании
  * YearsInCurrentRole - Количество лет в текущей должности
  * YearsSinceLastPromotion - Количество лет с даты последнего повышения
  * YearsWithCurrManager - Количество лет вместе с текущим менеджером
  * BusinessTravel - Категория частоты бизнес-поездок (часто или не часто ездит)
  * DistanceFromHome - Расстояние между домом и офисом в километрах
  * PerformanceRating - Оценка работы сотрудника (1-4, где 4 - превосходный результат)


### Задачи, которые решались с помощью дашборда:
1. Общий уровень разных видов удовлетворенности в компании в целом
2. и в разрезе департаментов и должностей.
3. Видеть, есть ли зависимости (уровня удовлетворенности от застоя сотрудников, т.е. сколько лет в текущей позиции, с текущим менеджером или с момента последнего повышения, от уровня зарплаты…).
4. Оценки каждого работника с возможностью сортировки по показателям.
5. Сравнивать оценки сотрудника со средним по его отделу и должности.
6. Переход на рабочую страницу сотрудника формата: hr-k.ru/idEmployeeNumber, для получения персональной детальной информации.
7. Основная версия – десктопная, дополнительно мобильная – для работы с дашбордом в командировке.

Подробнее о задачах и целях - в [Dashboard Canvas](https://gitlab.com/data-visualization8525117/dashboards-in-tableau/-/blob/main/empl_sat_Canvas_Natalie_Swan.pdf?ref_type=heads) в этом репозитории.
Если не открывается pdf, можно посмотреть [скрин документа](https://gitlab.com/data-visualization8525117/dashboards-in-tableau/-/blob/main/screenshots/canvas_dash_empl_sat.png?ref_type=heads)

![Dashboard Canvas](https://gitlab.com/data-visualization8525117/dashboards-in-tableau/-/blob/main/screenshots/canvas_dash_empl_sat.png?ref_type=heads)

### Стек

* Инструменты Tableau
* Power Point Presentation для разработки прототипа дашборда.


### Результат

[Основная версия дашборда](https://goo.su/yrL7f)

![Employee Satisfaction](https://gitlab.com/data-visualization8525117/dashboards-in-tableau/-/blob/main/screenshots/employee_sat_desk_c.png?ref_type=heads)

Так как scatter plot не показал корреляции между оценками сотрудников и исследуемыми факторами, решила вывести line chart с фильтром по этим факторам.

![Correlation](https://gitlab.com/data-visualization8525117/dashboards-in-tableau/-/blob/main/screenshots/correlation.png?ref_type=heads)


Нижнюю таблицу удобнее смотреть, выставляя общие фильтры по департаменту и должности (в правом верхнем углу дашборда).

Кроме десктопной адаптировала [мобильную версию дашборда](https://goo.su/pMHZE) (имеет смысл смотреть её через смартфон).

![qr-code](https://gitlab.com/data-visualization8525117/dashboards-in-tableau/-/blob/main/screenshots/qr_empl_sat_mobile.png?ref_type=heads)

![Мобильная версия](https://gitlab.com/data-visualization8525117/dashboards-in-tableau/-/blob/main/screenshots/empl_sat_mob_1.jpg?ref_type=heads)

## 2. Когортный анализ

Дашборд [Cohort analysis](https://public.tableau.com/app/profile/natalie.swan3088/viz/Cohortanalysis_17017947776800/Dashboard22?publish=yes)

![Cohort analysis](https://gitlab.com/data-visualization8525117/dashboards-in-tableau/-/blob/main/screenshots/dash_cohorts_tab_c.png?ref_type=heads)

**Цели:** 
* изучить среднюю еженедельную выручку от пользователей (ARPU), разделённых на когорты 
* изучить процент удержания пользователей (retention) в когортах на конец каждой недели

**Датасет** для проведения когортного анализа в Tableau предварительно подготовила в Python.

[Начальный датасет](https://disk.yandex.ru/d/wG4yI1hrTpC9fw) - data_for_tableau.csv –  разместила на ЯндексДиске. 

Для подготовки разделила пользователей на когорты по дате их первой сессии (границы - 1 неделя). В качестве индикатора использовала id девайса, с которого пользователь заходил. 

[Код](https://gitlab.com/data-visualization8525117/dashboards-in-tableau/-/blob/main/Cohort_analysis_in_Tableau_preprocessing.ipynb?ref_type=heads) находится в этом репозитории. 

В результате получилась такая таблица

![Prepared dataset](https://gitlab.com/data-visualization8525117/dashboards-in-tableau/-/blob/main/screenshots/prepared%20dataset%20for%20cohort%20analysis%20in%20tableau.png?ref_type=heads)

В колонке event имеем следующие уникальные статусы:
'app_start', 'choose_item', 'purchase', 'search', 'tap_basket',  'app_install', 'register'


Подготовленный датасет [data_for_cohort_analysis_in_tableau.csv](https://disk.yandex.ru/d/2AdeTh5yUtGtBA) также лежит на ЯндексДиске. 


### Стек

* Инструменты Tableau
* [Python]((https://gitlab.com/data-visualization8525117/dashboards-in-tableau/-/blob/main/Cohort_analysis_in_Tableau_preprocessing.ipynb?ref_type=heads)) 

### Результат
На [дашборде](https://public.tableau.com/app/profile/natalie.swan3088/viz/Cohortanalysis_17017947776800/Dashboard22?publish=yes) разместила 2 таблицы:

1. ![Retention](https://gitlab.com/data-visualization8525117/dashboards-in-tableau/-/blob/main/screenshots/cohort_an_retention.png?ref_type=heads)

2. ![Average revenue per user](https://gitlab.com/data-visualization8525117/dashboards-in-tableau/-/blob/main/screenshots/cohort_an_ARPU.png?ref_type=heads)
 


## 3. Обзор зарплат и требуемых навыков для аналитиков
[Дашборд What is the salary for an analyst in Russia?](https://goo.su/ekHW)

**Цель** - изучить количество вакансий и зарплатные предложения для аналитиков по четырём направлениям: 
* Data Engineering
* BI
* Data Science/Machine Learning
* Analytics

А также понять, какие требуются навыки и какие зарплатлы за них предлагают.

**Датасет**
Анализ основан на датасете с вакансиями аналитиков [HH_data.csv](https://gitlab.com/data-visualization8525117/dashboards-in-tableau/-/blob/main/HH_data_DA43.csv?ref_type=heads), которые нашлись по какому-то ключевому запросу на сайте hh с октября 2021 по сент 2023г.. Одна и та же вакансия может быть найдена по нескольким запросам.

### Стек

* Инструменты Tableau

### Результат

![Dashboard](https://gitlab.com/data-visualization8525117/dashboards-in-tableau/-/blob/main/screenshots/salary_c.png?ref_type=heads)

Все визуализации фильтруются по месяцам, направлениям и уровням (от линейного специалиста до директора). 

В фактоиды вынесла медианную зарплату и общее количество вакансий за весь период.

Так как данные за март 2023 года в таблице отсутствуют, на линейном графике показала разрыв данных.

На Word cloud chart навыки раскрашены в цвета в зависимости от зарплаты: от синего (самая низкая зарплата) до красного (самая высокая). А размер слов соответствует востребованности: чем крупнее шрифт, тем чаще навык требуют. Например, умение составлять дашборды запрашивали чуть чаще, чем умение работать в Tableau (3 556 раз против 3 336), но за навык Tableau предлагали более высокую зарплату. 

На диаграмме Salary Range показана зарплатная вилка для каждого из четырёх направлений. 

Справа от бар-чарта (медианная зарплата по направлениям) разместила спарклайны с динамикой вакансий по месяцам. 


## 4. Profits overview
[Profits overview by regions, states, categories](https://goo.su/owFwF0)

Это просто первая попытка из готовых визуализаций собрать дашборд. 

![Profits](https://gitlab.com/data-visualization8525117/dashboards-in-tableau/-/blob/main/screenshots/profits.png?ref_type=heads)


## My contacts
Если у вас появились вопросы или предложения по сотрудничеству, пожалуйста, напишите мне 
* в Телеграм @Natalie_l_Swan
* или на почту tasha.l.swan@gmail.com




